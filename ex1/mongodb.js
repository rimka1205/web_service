const { MongoClient } = require("mongodb");
// Connection URI
const uri =
  "mongodb://localhost";
// Create a new MongoClient
const client = new MongoClient(uri);
async function run() {
  try {
    // Connect the client to the server
    await client.connect();
    // Establish and verify connection
    const db = await client.db("Test").collection("test");
    console.log(await db.find({}).toArray());
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}
run().catch(console.dir);