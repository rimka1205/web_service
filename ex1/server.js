const express = require("express");
var bodyParser = require('body-parser')
const app = express();
const { MongoClient } = require("mongodb");
// Connection URI
const uri = "mongodb://localhost";
const client = new MongoClient(uri);

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

async function queryDB(params) {
	try {
		// Connect the client to the server
		await client.connect();
		// Establish and verify connection
		const db = await client.db("Test").collection("test");
		result = await db.find(params).toArray();
		//console.log(result)
	  } finally {
		// Ensures that the client will close when you finish/error
		//console.log(result)
		await client.close();
	}
	return result;
}

app.listen(3000, () => { 
	console.log("Example app listening on port 3000!");
})

app.get("/", async(req, res) => { 
	result = await queryDB(JSON.parse(req.query.search))
	return res.send(result)
});

app.post("/post", async(req, res) => { 
	result = await queryDB(req.body)
	return res.send(result)
});

app.put("/put", (req, res) => { 
	return res.send({"type": "PUT"});
});

app.delete("/delete", (req, res) => { 
	return res.send({"type": "DELETE"});
});
